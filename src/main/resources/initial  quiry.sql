INSERT INTO `example1`.`role` (`auth_Id`, `authority`) VALUES ('1', 'ADMIN');
INSERT INTO `example1`.`role` (`auth_Id`, `authority`) VALUES ('2', 'USER');

INSERT INTO `example1`.`user` (`userId`, `emailId`, `emailVerify`, `firstName`, `lastName`, `mobileNo`, `mobileVerify`, `status`, `accountNonLocked`, `requestDate`,`password`) VALUES ('2', 'admin@admin.com', 1, 'Admin', 'Admin', '1234567890', 1, 1, 1, CurrentDate(),'$2a$04$0ELmNp2rYyTfyBq7gGx39.Tnazg/JZpmSilGAh3T35gaZ2ah1zcny');


INSERT INTO `example1`.`user_role` (`users_userId`, `authorities_auth_Id`) VALUES ('1', '1');
