package com.example.demo.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailUtil {

	@Value("${email.subject}")
	private String subject;

	@Autowired
	private JavaMailSender emailSender;
	
	@Autowired
	private MessageSource messageSource;

	public void sendMail(String toEmail, String name, String userId) {
		String url = "http://localhost:8080/registration/validate?userId="+userId;
		String mailTemplate = messageSource.getMessage("mail.template", new Object[]{name,url}, null);

		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(toEmail);
		message.setSubject(subject);
		message.setText(mailTemplate);
		emailSender.send(message);
	}
}
