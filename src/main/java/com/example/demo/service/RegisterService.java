package com.example.demo.service;

import org.springframework.validation.BindingResult;

import com.example.demo.dto.UserDto;

public interface RegisterService {
	
	boolean registerUser(UserDto userDto, BindingResult result) throws Exception;
	
	boolean validateEmail(String userId) throws Exception;
	
	boolean lockUnlockAccount(String userId) throws Exception;

}
