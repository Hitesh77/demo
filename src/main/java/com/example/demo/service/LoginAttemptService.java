package com.example.demo.service;

import com.example.demo.entity.UserAttempts;

public interface LoginAttemptService {
	
	void updateFailAttempts(String userName, String remoteAddress) throws Exception;
	
	void resetFailAttempts(String userName) throws Exception;
	
	UserAttempts getUserAttempts(String username) throws Exception;


}
