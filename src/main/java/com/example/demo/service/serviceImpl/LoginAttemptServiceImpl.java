package com.example.demo.service.serviceImpl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.User;
import com.example.demo.entity.UserAttempts;
import com.example.demo.repository.UserAttemptsRepository;
import com.example.demo.service.LoginAttemptService;
import com.example.demo.service.UserService;

@Service
@PropertySource("classpath:config.properties")
public class LoginAttemptServiceImpl implements LoginAttemptService {

	@Autowired
	private UserAttemptsRepository userAttemptsRepository;

	@Autowired
	private UserService userService;

	@Value("${max.login.failure.allowed}")
	private int MAX_ATTEMPT;

	@Override
	public void updateFailAttempts(String userName, String remoteAddress) throws Exception {
		try {
			if (StringUtils.isNumeric(userName)) {
				User user = userService.getByEmailOrMobileNo(userName);
				userName = user.getEmailId();
			}

			UserAttempts userAttempts = userAttemptsRepository.getByUserName(userName);
			if (userAttempts == null) {
				userAttempts = new UserAttempts();
				userAttempts.setUserName(userName);
				userAttempts.setRemoteAddress(remoteAddress);
				userAttempts.setAttempts(1);
			} else {
				userAttempts.setAttempts(userAttempts.getAttempts() + 1);
			}

			if (userAttempts.getAttempts() >= MAX_ATTEMPT) {
				User user = userService.getByEmailId(userAttempts.getUserName());
				user.setAccountNonLocked(false);
				userService.update(user);
			}

			userAttemptsRepository.save(userAttempts);
		} catch (Exception e) {
		}
	}

	@Override
	@Transactional
	public void resetFailAttempts(String userName) throws Exception {
		try {
			if (StringUtils.isNumeric(userName)) {
				User user = userService.getByEmailOrMobileNo(userName);
				userName = user.getEmailId();
			}
			userAttemptsRepository.deleteByUserName(userName);
		} catch (Exception e) {
		}
	}

	@Override
	public UserAttempts getUserAttempts(String username) throws Exception {
		try {
			if (StringUtils.isNumeric(username)) {
				User user = userService.getByEmailOrMobileNo(username);
				username = user.getEmailId();
			}
		} catch (Exception e) {
		}
		return userAttemptsRepository.getByUserName(username);
	}

}
