package com.example.demo.service.serviceImpl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = null;
		try {
			user = getByEmailOrMobileNo(email);
			if (user == null) {
				throw new UsernameNotFoundException(email);
			}
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
		return user;
	}

	@Override
	public User getByEmailOrMobileNo(String userName) throws UsernameNotFoundException {
		User user = null;
		try {
			if (StringUtils.isNumeric(userName)) {
				user = userRepository.findByMobileNo(Long.valueOf(userName));
			} else {
				user = userRepository.findByEmailId(userName);
			}
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
		return user;
	}

	@Override
	public User getByEmailId(String emailId) throws UsernameNotFoundException {
		User user = null;
		try {
			user = userRepository.findByEmailId(emailId);
		} catch (Exception e) {
		}
		return user;
	}

	@Override
	public User getByMobileNo(Long MobileNo) throws UsernameNotFoundException {
		User user = null;
		try {
			user = userRepository.findByMobileNo(MobileNo);
		} catch (Exception e) {
		}
		return user;
	}

	@Override
	public User update(User user) throws Exception {
		try {
			return userRepository.save(user);
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public User saveUser(User user) throws Exception {
		userRepository.save(user);
		return null;
	}

	@Override
	public List<User> getAllUsers() throws Exception {
		List<User> users = userRepository.findAll();
		return users;
	}

}
