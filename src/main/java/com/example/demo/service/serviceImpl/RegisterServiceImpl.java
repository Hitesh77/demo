package com.example.demo.service.serviceImpl;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.LoginAttemptService;
import com.example.demo.service.RegisterService;
import com.example.demo.service.UserService;
import com.example.demo.utility.EmailUtil;
import com.example.demo.utility.PasswordEncoderUtility;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private EmailUtil emailUtil;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private LoginAttemptService loginAttemptService;
	
	@Value("${validate.expiration.time}")
	private long validationExpTime;

	@Override
	public boolean registerUser(UserDto userDto, BindingResult result) throws Exception {
		boolean userCreated = true;
		try {
			boolean userValid = validateUser(userDto, result);

			if (userValid) {

				User user = userFound(userDto);
				if (user != null) {
					if (!user.isStatus()) {
						if(getDateDiff(user.getRequestDate()) > 180) {
							userCreated = false;
							result.rejectValue("firstName", "account.validate");
							return userCreated;
						}
						user.setRequestDate(new Date(new java.util.Date().getTime()));
					}

				} else {
					user = setUserDto(userDto);
					userCreated = true;
				}
				userRepository.save(user);
				emailUtil.sendMail(user.getEmailId(), user.getFirstName(), user.getUserId());
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return userCreated;
	}

	private boolean validateUser(UserDto userDto, BindingResult result) {
		boolean userValid = true;

		if (StringUtils.isBlank(userDto.getFirstName())) {
			result.rejectValue("firstName", "error.firstName");
			userValid = false;
		}

		if (StringUtils.isBlank(userDto.getLastName())) {
			result.rejectValue("lastName", "error.lastName");
			userValid = false;
		}

		if (StringUtils.isBlank(userDto.getEmailId())) {
			result.rejectValue("emailId", "error.emailId");
			userValid = false;
		}

		if (userDto.getMobileNo() == null) {
			result.rejectValue("mobileNo", "error.mobileNo");
			userValid = false;
		}

		if (StringUtils.isBlank(userDto.getAddress())) {
			result.rejectValue("address", "error.address");
			userValid = false;
		}

		if (StringUtils.isBlank(userDto.getPassword())) {
			result.rejectValue("password", "error.password");
			userValid = false;
		}

		if (StringUtils.isBlank(userDto.getConfirmPassword())) {
			result.rejectValue("confirmPassword", "error.confirmPassword");
			userValid = false;
		}

		if (StringUtils.isNotBlank(userDto.getPassword()) && StringUtils.isNotBlank(userDto.getConfirmPassword())
				&& !StringUtils.equals(userDto.getPassword(), userDto.getConfirmPassword())) {
			result.rejectValue("confirmPassword", "error.password.not.match");
			result.rejectValue("password", "error.password.not.match");
			userValid = false;
		}

		return userValid;
	}

	private User userFound(UserDto userDto) {
		User user = null;
		try {
			user = userService.getByEmailId(userDto.getEmailId());
			if (user == null) {
				user = userService.getByMobileNo(userDto.getMobileNo());
			}

		} catch (Exception e) {

		}
		return user;
	}

	private User setUserDto(UserDto userDto) {
		User user = new User();
		user.setEmailId(userDto.getEmailId());
		user.setEmailVerify(false);
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setMobileNo(userDto.getMobileNo());
		user.setMobileVerify(false);
		user.setPassword(PasswordEncoderUtility.encode(userDto.getPassword()));
		user.setStatus(false);
		user.setAccountNonLocked(true);
		user.setRequestDate(new Date(new java.util.Date().getTime()));
		Role role = roleRepository.findAll().get(1);
		Set<Role> auth = new HashSet<Role>();
		auth.add(role);
		user.setAuthorities(auth);
		return user;
	}

	@Override
	public boolean validateEmail(String userId) throws Exception {
		User user = userRepository.findById(userId).get();
		
		long currentDate = System.currentTimeMillis();
		if (((user.getRequestDate().getTime()) - currentDate) > (validationExpTime * 60 * 60 *1000) ) {
			return false;
		}
		user.setEmailVerify(true);
		user.setStatus(true);
		userRepository.save(user);
		return true;
	}
	
	private int getDateDiff(Date date) {
		long diff = date.getTime() - new java.util.Date().getTime();
		return (int)diff / ( 24 *60 * 60 *1000);
		
	}

	@Override
	public boolean lockUnlockAccount(String userId) throws Exception {
		User user = userRepository.findById(userId).get();
		if(user != null) {
			user.setAccountNonLocked(user.isAccountNonLocked() ? false : true);
			userRepository.save(user);
			loginAttemptService.resetFailAttempts(user.getEmailId());
		}
		return true;
	}

}
