package com.example.demo.service;

import java.util.List;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.example.demo.entity.User;

public interface UserService {
	
	User getByEmailOrMobileNo(String userName) throws UsernameNotFoundException;
	
	User getByEmailId(String emailId) throws UsernameNotFoundException;
	
	User getByMobileNo(Long MobileNo) throws UsernameNotFoundException;
	
	User update(User user)throws Exception;
	
	User saveUser(User user) throws Exception;
	
	List<User> getAllUsers() throws Exception;

}
