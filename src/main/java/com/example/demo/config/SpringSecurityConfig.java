package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.example.demo.service.serviceImpl.UserServiceImpl;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.example.demo")
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

	@Override
	protected void configure(final HttpSecurity http) throws Exception {

		http.csrf().disable()
			.authorizeRequests()
				.antMatchers("/", "/login", "/static/js/**", "/static/css/**","/registration/**","/user/**").permitAll()
				.antMatchers("/dashboard/**").authenticated()
				.anyRequest().authenticated()
			.and()
			.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/dashboard", true)
				.failureUrl("/login?error")
				.failureHandler(authenticationFailureHandler)
			.and()
			.logout()
				.deleteCookies("JSESSIONID")
				.invalidateHttpSession(true)
				.logoutSuccessUrl("/login?logout");

	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {

		
		/*
		 * auth.inMemoryAuthentication()
		 * .withUser("user").password(passwordEncoder().encode("password")).roles(
		 * "USER") .and()
		 * .withUser("admin").password(passwordEncoder().encode("password")).roles(
		 * "ADMIN") .and()
		 * .withUser("hitesh").password(passwordEncoder().encode("password")).roles(
		 * "ADMIN");
		 */
		 

		auth.authenticationProvider(authenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userServiceImpl);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
