package com.example.demo.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.example.demo.entity.UserAttempts;
import com.example.demo.service.LoginAttemptService;

@Component("authenticationFailureHandler")
@PropertySource("classpath:config.properties")
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Value("${max.login.failure.allowed}")
	private int MAX_ATTEMPT;

	@Autowired
	private LoginAttemptService loginAttemptService;

	@Autowired
	private MessageSource messageSource;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		try {
			setDefaultFailureUrl("/login?error=true");
			super.onAuthenticationFailure(request, response, exception);

			UserAttempts userAttempts;

			userAttempts = loginAttemptService.getUserAttempts(request.getParameter("username"));

			String errorMessage = messageSource.getMessage("invalid.user.name.password", null, null);

			if (userAttempts.getAttempts() >= MAX_ATTEMPT) {
				errorMessage = messageSource.getMessage("lock.user.account", null, null);
			}

			request.getSession().setAttribute("errorMessage", errorMessage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
