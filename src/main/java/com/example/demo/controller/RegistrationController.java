package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.dto.UserDto;
import com.example.demo.entity.User;
import com.example.demo.service.RegisterService;

@Controller
public class RegistrationController {
	
	@Autowired
	private RegisterService registerService;

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView getLoginPage() {
		ModelAndView view = new ModelAndView("registration");
		view.addObject("user", new UserDto());
		return view;
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") UserDto user, BindingResult result, HttpServletRequest request) {
		ModelAndView view = new ModelAndView("registration");
		try {
		boolean userCreated;
			userCreated = registerService.registerUser(user, result);
		if(userCreated) {
			view.setViewName("redirect:login?registered");
		}
		view.addObject("user", user);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return view;
		
	}
	
	@RequestMapping(value = "/registration/validate", method = RequestMethod.GET)
	public ModelAndView validateEmail(@RequestParam String userId) {
		ModelAndView view = new ModelAndView("login");
		try {
			boolean validate = registerService.validateEmail(userId);
			view.setViewName("redirect:login?registered");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return view;
	}
	
	@RequestMapping(value = "/user/lockUnlock", method = RequestMethod.POST)
	public void lockUnlockAccount(@RequestBody User user) {
		try {
			boolean validate = registerService.lockUnlockAccount(user.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
