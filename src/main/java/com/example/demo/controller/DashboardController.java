package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;


@Controller
public class DashboardController {
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView getLoginPage() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		List<GrantedAuthority> authority = (List<GrantedAuthority>) userDetails.getAuthorities();
		if (StringUtils.equalsIgnoreCase(authority.get(0).getAuthority(),"ADMIN")) {
			return new ModelAndView("dashboard");
		}else {
			return new ModelAndView("home");
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/dashboard/getAllUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
	public List<User> getAllMessages(){
		List<User> users = null ;
		try {
			users =  userService.getAllUsers();
		} catch (Exception e) {
			System.out.println(e);
		}
		return users != null ? users : new ArrayList<User>();
	}

}
