package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.UserAttempts;

public interface UserAttemptsRepository extends JpaRepository<UserAttempts, Integer>{

	UserAttempts getByUserName(String userName);
	
	void deleteByUserName(String userName);
}
