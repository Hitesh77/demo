var table;
$(document).ready(function(){
	table = $('#dashboardTable').DataTable(
	{
		"sAjaxSource" : "/dashboard/getAllUser",
		"sAjaxDataProp" : "",
		"lengthChange" : false,
		"pageLength": 8,
		"dom": 'Bfrtip',
		"scrollX": true,	
        "columns" : [ 
		    		 {"data" : "firstName"}, 
		    		 {"data" : "lastName"}, 
		    		 {"data" : "emailId"},
		    		 {"data" : "emailVerify"},
		    		 {"data" : "mobileNo"},
		    		 {"data" : "mobileVerify"},
		    		 {"data" : "address"},
		    		 {"data" : "accountNonLocked"},
		    		 {"data" : "status"},
		    		 {"targets": -1,
			             "data": null,
			             "defaultContent": ''
		    		 }
    		 ],
    	"aoColumnDefs" : [
    			{
    				"aTargets": [8],
    				"mData" :"status",
    				"mRender": function(data,type,full){
    					if(full.status){
    						return "Active";
    					}
    					else{
    						return "Not Active";
    					}
    				}
    			},{
    				"aTargets": [9],
    				"mData" :"accountNonLocked",
    				"mRender": function(data,type,full){
    					if(full.accountNonLocked){
    						return '<a href="#" id="lock" >Lock Account</a>';
    					}
    					else{
    						return '<a href="#" id="lock" >UnLock Account</a>';
    					}
    				}
    			}]
	});
	
	$('#dashboardTable tbody').on('click','a[id*="lock"]', function() {
		var data = table.row($(this).parents('tr')).data();
		
		lockUnlockUser(data.userId);
	});
	
})

function lockUnlockUser(userId){
	console.log(userId);
	var url = "http://localhost:8080/user/lockUnlock";
	var method = "POST";
	var dataType ="json";
	var contentType ="application/json";
	
	var data ={
			"userId" :userId,
	}
	
	var xhr = new XMLHttpRequest();
	xhr.open(method, url, true);
	xhr.setRequestHeader("Content-type", contentType);
//	xhr.setRequestHeader(csrfHeader, csrfToken);
	xhr.send(JSON.stringify(data));
	xhr.onreadystatechange = function() {
		table.ajax.reload();
	}
}
