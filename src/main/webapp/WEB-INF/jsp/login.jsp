<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE >
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Login</title>
	<link rel="shortcut icon" type="image/png" href="<c:url value="/static/images/favicon.png"/>">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" src="<c:url value="/static/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/bootstrap/bootstrap.min.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/bootstrap/bootstrap.min.css"/>" />
</head>
	
<body style="background-color:#114861">
 <p>&nbsp;</p>
 <div class="container py-5">
  <div class="row">
   <div class="col-md-5 mx-auto">
    <div class="card rounded-2" style="background-color: #ffffff;">
     <div class="card-body">
      <h2 class="mb-1" style="text-align: center;"><strong>Demo</strong></h2>
      <hr>
      
      <c:if test="${sessionScope.errorMessage != null}">
       <div id="errorDiv" class="alert alert-danger alert-dismissable" >
        <button type="button" class="close" onclick="$('#errorDiv').prop('hidden', true);">&times;</button>
        <strong>Error!</strong>
        <label><c:out value="${sessionScope.errorMessage}"/> </label>
       </div>
      </c:if>
      
      <c:if test="${param.logout != null}">
       <div id="successDiv" class="alert alert-success alert-dismissable" >
        <button type="button" class="close" onclick="$('#successDiv').prop('hidden', true);">&times;</button>
        <strong>Success! </strong>
        <label>You've been logged out successfully.</label>
       </div>
      </c:if>
      
      <c:if test="${param.registered != null}">
       <div id="successDiv" class="alert alert-success alert-dismissable" >
        <button type="button" class="close" onclick="$('#successDiv').prop('hidden', true);">&times;</button>
        <label style="display: inline;"><strong>Success! </strong>Please click on the link that has just sent to your email account to verify your email and continue the registration process.</label>
       </div>
      </c:if>
      
      <form id="loginPage" action="login" method="post" class="was-validated">
       
       <div class="form-group has-feedback" style="padding:2%">
        <input type="text" class="form-control " name="username" placeholder="Enter UserName" required="required">
       </div>
       
       <div class="form-group" style="padding:2%">
        <input type=password class="form-control"  name="password" placeholder="Enter Password" required="required">
       </div>
										
       <div>
       	<button type="button" class="btn btn-outline-info" onclick="location.href='/registration';">Sign Up</button>
        <button type="submit" class="btn btn-outline-info float-right" >Login</button>
       </div>
       
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>	
</body>
		
<style type="text/css">
 .btn-outline-info{
	color:#114861 !important;
	border-color:#114861 !important;
 }
 .btn-outline-info:hover{
	background-color:#114861 !important;
	color:#ffffff !important;
 } 
</style>
	
</html>