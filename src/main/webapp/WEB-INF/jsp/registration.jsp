<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Registration</title>
	<link rel="shortcut icon" type="image/png" href="<c:url value="/static/images/favicon.png"/>">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" src="<c:url value="/static/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/bootstrap/bootstrap.min.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/bootstrap/bootstrap.min.css"/>" />

</head>
<body style="background-color:#114861">

 <div class="container py-5">
  <div class="row">
   <div class="col-md-10 mx-auto">
    <div class="card rounded-2" style="background-color: #ffffff;">
     <div class="card-body">
      <h2 class="mb-1" style="text-align: center;"><strong>Registration Form</strong></h2>
      <hr>
      
      <form:form id="userRegistration" modelAttribute="user" action="registration" method="post" class="was-validated">
       
      <div class="row">
       <div class="form-group has-feedback col-md-6" >
       	<label for="uname">First Name:</label>
       	<form:input path="firstName" class="form-control" placeholder="Enter FirstName" required="required" />
       	<form:errors path="firstName" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
       	<label for="uname">Last Name:</label>
       	<form:input path="lastName" class="form-control" placeholder="Enter LastName" required="required" />
       	<form:errors path="lastName" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
       	<label for="uname">Email Id:</label>
       	<form:input path="emailId" class="form-control" placeholder="Enter Email Id" required="required" 
       				pattern="^[A-Za-z0-9]+@[A-Za-z0-9]+.[A-Za-z]{2,4}" />
       	<form:errors path="emailId" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
       	<label for="uname">Mobile Number:</label>
       	<form:input path="mobileNo" class="form-control" placeholder="Enter Mobile Number" required="required" 
       				maxlength="10" pattern="^[0-9]{10}" />
       	<form:errors path="mobileNo" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
       	<label for="uname">Address:</label>
       	<form:input path="address" class="form-control" placeholder="Enter Address" required="required" />
       	<form:errors path="address" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
        <label for="uname">Password:</label>
        <form:password path="password" class="form-control" placeholder="Enter Password" required="required" />
        <form:errors path="password" cssClass="text-danger"/>
       </div>
       
       <div class="form-group has-feedback col-md-6" >
        <label for="uname">Confirm Password:</label>
        <form:password path="confirmPassword" class="form-control" placeholder="Enter Confirm Password" required="required" />
        <form:errors path="confirmPassword" cssClass="text-danger"/>
       </div>
       
       <div class="col-12">
       	<button type="submit" class="btn btn-outline-info float-right" >Register</button>
       </div>
      
      </div>
      
      </form:form>
 
     </div>
    </div>
   </div>
  </div>
 </div>

</body>

<style type="text/css">
 .btn-outline-info{
	color:#114861 !important;
	border-color:#114861 !important;
 }
 .btn-outline-info:hover{
	background-color:#114861 !important;
	color:#ffffff !important;
 } 
</style>

<script type="text/javascript">
$( document ).ready(function() {
	$('#confirmPassword').keyup(function () {  
		validatePassword();
	});
	
	$('#password').keyup(function () {  
		validatePassword();
	});
});

function validatePassword(){
	if($('#password').val() != $('#confirmPassword').val()){
		confirmPassword.setCustomValidity("Confirm Passwords Don't Match");
	} else{
		confirmPassword.setCustomValidity("");
	}
}
</script>

</html>