<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dashboard</title>
	<link rel="shortcut icon" type="image/png" href="<c:url value="/static/images/favicon.png"/>">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" src="<c:url value="/static/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/bootstrap/bootstrap.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/dashboard.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/bootstrap/bootstrap.min.css"/>" />
	<!-- DataTables -->
	<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/datatable/dataTables.bootstrap.min.css"/>" />
	<script type="text/javascript" src="<c:url value="/static/js/datatable/jquery.dataTables.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/datatable/dataTables.bootstrap.min.js"/>"></script>
	
</head>
<body>

 <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
  <ul class="navbar-nav">
   <li class="nav-item active">
    <a class="nav-link" href="#">Demo Application</a>
   </li>
  </ul>
  <ul class="navbar-nav ml-auto">
  	<a class="nav-link active" href="/logout">Logout</a>
  </ul>
 </nav>

 
 <div class="container" style="width: 100%;margin: 0 auto; margin-top:90px ">
 <div class="row">
 <div class="col-xs-12">
 <div class="box">
 <div class="box-body">
 
  <table id="dashboardTable" class="table table-bordered table-striped" width="100%" Style="text-align:center;">
   <thead>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email Id</th>
    <th>Email Id Verified</th>
    <th>Mobile Number</th>
    <th>Mobile Number Verified</th>
    <th>Address</th>
    <th>Account Not Lock</th>
    <th>Status</th>
    <th>Action</th>
   </thead>
   <tbody>
   </tbody>
  </table>
  </div>
  </div>
  </div>
  </div>
 </div>

</body>
</html>