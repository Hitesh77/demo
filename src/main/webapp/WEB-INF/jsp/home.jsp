<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dashboard</title>
	<link rel="shortcut icon" type="image/png" href="<c:url value="/static/images/favicon.png"/>">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script type="text/javascript" src="<c:url value="/static/js/jquery.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/static/js/bootstrap/bootstrap.min.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/static/css/bootstrap/bootstrap.min.css"/>" />
</head>
<body>

 <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
  <ul class="navbar-nav">
   <li class="nav-item active">
    <a class="nav-link" href="#">Demo Application</a>
   </li>
  </ul>
  <ul class="navbar-nav ml-auto">
  	<a class="nav-link active" href="/logout">Logout</a>
  </ul>
 </nav>

 
 <div class="container text-white" style="width: 90%;margin: 0 auto; margin-top:90px ">
	<label>Hi </label> <strong ><sec:authentication property="principal.firstName" /></strong> <label> welcome to the DEMO Application.</label>
 </div>

</body>
</html>